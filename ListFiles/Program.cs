﻿using System;
using System.IO;


namespace ListFiles
{
    internal class Program
    {
        private String Root { get; set; } = null;


        static void Main(string[] args)
        {
            Program prog = new Program(args);
            prog.Run();
        }


        public Program(string[] args)
        {
            if (args.Length < 1)
                Root = Directory.GetCurrentDirectory();
            else
                Root = args[args.Length - 1];
        }


        public void Run()
        {
            PrintFiles(Root);
        }


        private void PrintFiles(string path)
        {
            string[] entries;
            try
            {
                entries = Directory.GetFileSystemEntries(path);
                Array.Sort(entries);
            }
            catch (UnauthorizedAccessException)
            {
                return;
            }

            Console.WriteLine(path);

            foreach (var i in entries)
            {
                var attr = File.GetAttributes(i);
                if (attr.HasFlag(FileAttributes.Directory) && ! attr.HasFlag(FileAttributes.ReparsePoint))
                    PrintFiles(i);
                else
                    Console.WriteLine(i);
            }
        }
    }
}
